


class Student {
    // instance variable
    String name;
    
    // instance methods
    void methodName() {
        System.out.println("method called");
    }

    // constructors
}


public class T002ClassObjectBasics {
    public static void main(String[] args) {

        Student std = new Student();
        std.name = "Bob";
        std.methodName();

    }
}