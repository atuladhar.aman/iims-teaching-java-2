
class Student {
    String name;
    Student() {
        this("defaultName");
    }

    Student(String name) {
        this.name = name;
    }
}

public class Main {
    public static void main(String[] args) {
        Student s = new Student();
        System.out.println(s.name);

        Student s2 = new Student("customName");
        System.out.println(s2.name);

    }
}