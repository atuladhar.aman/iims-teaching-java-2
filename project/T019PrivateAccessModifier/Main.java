

class Student {
   private String name;
   void setName(String name) {
       this.name = name;
   }
   //because we cannot access name
   //from outside of class we created 
   //a method that returns name
   String getName() {
       return name;
   }
}
public class Main {
    public static void main(String[] args) {
        Student s = new Student();
        s.setName("StdName");
        System.out.println(s.getName());
    }
}