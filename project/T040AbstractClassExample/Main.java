
abstract class AbstractClass {
    abstract void abstractMethod();
}

class Child extends AbstractClass {
    void abstractMethod() {
        System.out.println("Overriden abstract method");
    }
}

public class Main {
    public static void main(String[] args) {
        
    }
}