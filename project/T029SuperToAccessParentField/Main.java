
class Parent {
    String parentField;
}

class Child extends Parent{
    String childField;

    Child(){
        super.parentField = "parentField";
        this.childField = "childField";
    }

    void callMe() {
        System.out.println(super.parentField);
        System.out.println(this.childField);
    }
}

public class Main {
    public static void main(String[] args) {
        Child c = new Child();
        c.callMe();
    }
}