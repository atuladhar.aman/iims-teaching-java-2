

/**

WAP where user is given a menu for CRUD operation to store Student information.
When users selects the options, operation should perform the task user selected
Student must have attributes: age, name, dob
Setting the state of an object should be done using methods / behaviors
Array size to hold the Student should be 10
 */
class Student {
    // state like age, name, dob
    // methods to set age, name , dob
}

public class A001StudentCrud {
    public static void main(String[] args) {

        Student[] listOfStudent = new Student[10];
        listOfStudent[0] = new Student();
        
    }
}