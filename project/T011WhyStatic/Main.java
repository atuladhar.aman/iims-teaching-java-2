

class Student {
    String name;
    String collegeName = "IIMS";
}


public class Main {
    public static void main(String[] args) {
        Student s1 = new Student();
        ... // Here a collegeName is created
        ... // for each an every object
        ... // eventhought they hold same value
        ... // because they are declared as fields
        Student s1000 = new Student();
    }
}