

class Student {
    String name;
    static Integer count = 0;
    Student() {
        Student.count++;
    }
}
public class Main {
    public static void main(String[] args) {
        Student s1 = new Student();
        Student s2 = new Student();
        System.out.println(Student.count);
    }
}