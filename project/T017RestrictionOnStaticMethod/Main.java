

class Student {
    String name;
    void instanceMethod() {}

    static void callMe() {
        // name is instance variable
        // cannot access instance variable
        // from static method
        name = "this is also error";

        // instanceMethod is instance method
        // cannot call from static reference
        instanceMethod();

        // this is instance reference
        // cannot access this from static method
        this.name = "this is error";

        // there is someting called `super`
        // which cannot use
        // will be covered in inheritance
    }

}
public class Main {
    public static void main(String[] args) {
        Student.callMe();        
    }
}