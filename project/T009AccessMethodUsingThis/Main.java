
class Student {
    void callMeWithThis() {
        System.out.println("callMeWithThis");
    }
    void callMe() {
        this.callMeWithThis();
    }
}

public class Main {
    public static void main(String[] args) {
        Student s = new Student();
        s.callMe();

        // Ofcourse we can call callMeWithThis() here
        s.callMeWithThis();
    }
}