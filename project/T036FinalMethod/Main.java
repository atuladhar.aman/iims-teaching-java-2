
class Parent {
    final void cannotOverrideMe() { /* BODY */ }
}
class Child extends Parent{
    // this is compile time error
    void cannotOverrideMe() { /* BODY */}
}


public class Main {
    public static void main(String[] args) {
        


    }
}