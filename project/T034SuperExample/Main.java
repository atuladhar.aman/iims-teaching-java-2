class Person{  
    int id; 
    String name;  
    Person(int id,String name){  
        this.id=id; 
        this.name=name;  
    }  
}  
class Student extends Person{  
    String  semester;  
    Student(int id, String name, String semester){  
        super(id, name);//reusing parent constructor  
        this.semester=semester;  
    }  
    void display(){
        System.out.println(id+" "+name+" "+semester);
    }  
}  
public class Main{  
    public static void main(String[] args){  
        Student s1=new Student(1,"ABC","3rd");  
        s1.display();  
    }
}  