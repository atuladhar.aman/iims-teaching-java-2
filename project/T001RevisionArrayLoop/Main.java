import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] names = new String[5];
        String userChoice;
        do {
            System.out.println("A. Show All");
            System.out.println("B. Show by index");
            System.out.println("C. Add");
            System.out.println("D. Update by index");
            System.out.println("E. Delete");

            System.out.println();
            System.out.println();
            System.out.println();

            System.out.print("What is your choice: ");
            userChoice = scan.nextLine();

            System.out.println("-------------------------");

            switch(userChoice) {
                case "A":
                case "a":
                    for(int i = 0; i < 5 ; i++){
                        if(names[i] != null){
                            System.out.println("["+i+"] = " + names[i]);
                        }
                    }
                    break;
                case "B":
                case "b":
                    System.out.print("Which position: ");
                    int positionToDisplay = scan.nextInt();
                    scan.nextLine(); // clear enter that was pressed from buffer
                    System.out.println(names[positionToDisplay]);
                    break;
                case "C":
                case "c":
                    System.out.print("Enter a name to add to list: ");
                    String nameToAdd = scan.nextLine();
                    System.out.println("Which position do you want to add: ");
                    int positionToAdd = scan.nextInt();
                    scan.nextLine();
                    names[positionToAdd] = nameToAdd;
                    break;
                case "D":
                case "d":
                    System.out.print("Which position do you want to update: ");
                    int positionToUpdate = scan.nextInt();
                    scan.nextLine();
                    if(names[positionToUpdate] == null){
                        System.out.println("Item doesn't exist on position you selected");
                        break;
                    }
                    System.out.println("You are going to update " + names[positionToUpdate] + ". Do you want that?");
                    userChoice = scan.nextLine();
                    if(userChoice.equalsIgnoreCase("y")){
                        System.out.print("Enter a updated name: ");
                        String updatedName = scan.nextLine();
                        names[positionToUpdate] = updatedName;
                    }
                    break;
                case "E":
                case "e":
                    System.out.print("Which position do you want to delete: ");
                    int positionToDelete = scan.nextInt();
                    scan.nextLine();
                    if(names[positionToDelete] == null){
                        System.out.println("Item doesn't exist on position you selected");
                        break;
                    }
                    System.out.println("You are going to delete " + names[positionToDelete] + ". Do you want that?");
                    userChoice = scan.nextLine();
                    if(userChoice.equalsIgnoreCase("y")){
                        String updatedName = scan.nextLine();
                        names[positionToDelete] = null;
                    }
                    break;
            }
            System.out.print("Do you want to continue: ");
            userChoice = scan.nextLine(); 
        }while(userChoice.equalsIgnoreCase("y"));
        scan.close();
    }
}