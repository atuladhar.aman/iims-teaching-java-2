


class Student {

    String methodThatReturnString(){
        return "Hello World";
    }

    int methodThatReturnInt() {
        return 10;
    }

    void methodThatAcceptsTwoArgument(int x, int y){
        int sum = x + y;
        System.out.println(sum);
    }

    int methodThatReturnTheSumOfTwoArgs(int x, int y){
        return x + y;
    }

}


public void T003Methods {
    public static void main(String[] args) {

        //creating object becuase we will
        // be calling instance methods
        Student std = new Student();

        // if method has return type we can capture
        // returned value on variable / object
        String returnedString = std.methodThatReturnString();
        int returnedInt = std.methodThatReturnInt();

        // if method doesn't return anything
        // we just invoke it, no value is captured
        std.methodThatAcceptsTwoArgument(10, 20);
        int sum = std.methodThatReturnTheSumOfTwoArgs(10, 20);
        
    }
}