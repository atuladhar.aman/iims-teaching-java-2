

class Student {
    String name;

    // no return type as method just sets state
    // simple method name that describes its purpose
    // accepts an argument that is assigned as object state
    void setName(String studentName) {
        // name can easily be used like this
        name = studentName;
    }
}


public class T004SettingObjectStateUsingMethods {
    public static void main(String[] args) {
     
        Student std = new Student();
        std.setName("studentName");

        // checking if student name is set
        System.out.println("Student name: " + std.name);

        Student std2 = new Student();
        std.setName("secondName");
        System.out.println("Student two name: " + std2.name);
        
    }
}