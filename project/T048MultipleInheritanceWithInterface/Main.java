interface Printable{  
    void print();  
}  
interface Showable{  
    void show();  
}  
class Child implements Printable,Showable{  
    public void print(){System.out.println("Hello");}  
    public void show(){System.out.println("Welcome");}   
} 
public class Main {
    public static void main(String args[]){  
        A7 obj = new A7();  
        obj.print();  
        obj.show();  
    }  
} 