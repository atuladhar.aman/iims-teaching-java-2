class Bank{  
    int getRateOfInterest(){return 0;}  
}        
class NIBL extends Bank{  
    int getRateOfInterest(){return 8;}  
}     
class NIC extends Bank{  
    int getRateOfInterest(){return 7;}  
}  
class Prime extends Bank{  
    int getRateOfInterest(){return 9;}  
}  
      
public class Main{  
    public static void main(String args[]){  
        NIBL s=new NIBL();  
        NIC i=new NIC();  
        Prime a=new Prime();  
        System.out.println("NIBL Rate of Interest: "+s.getRateOfInterest());  
        System.out.println("NIC Rate of Interest: "+i.getRateOfInterest());  
        System.out.println("Prime Rate of Interest: "+a.getRateOfInterest());  
    }  
} 