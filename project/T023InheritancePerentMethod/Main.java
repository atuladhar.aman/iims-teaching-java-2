
class Person {
    void parent(){System.out.println("parent");}
}
class Teacher extends Person {
    void teacher(){System.out.println("teacher");}
}
class Student extends Person {
    void student(){System.out.println("student");}
}

public class Main {
    public static void main(String[] args) {
        Student s = new Student();
        s.parent();
        s.student();
        // s.teacher() not accessable

        Teacher t = new Teacher();
        t.parent();
        t.teacher();
        // t.student() not accessable
    }
}

    

    