
class Person { String name; }

class Teacher extends Person { String subject; }

class Student extends Person { String semester; }

public class Main {
    public static void main(String[] args) {
        Student s = new Student();
        s.name = "stdName";
        s.semester = "3rd";
        // s.subject not accessable

        Teacher t = new Teacher();
        t.name = "teacherName";
        t.subject = "JAVA";
        // t.semester not accessable
    }
}

    

    