

abstract class AbstractClass {
    // abstract class can only be declared
    // in abstract class or interface
    // abstract class doesn't have any body
    // i.e no curly braces, and ends with semicolon
    abstract void abstractMethod();
}