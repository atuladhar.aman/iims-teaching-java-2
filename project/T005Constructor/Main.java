

class StudentDefault {
    String name;
}

class StudentNoArgs {
    String name;
    StudentNoArgs() {
        name = "NO ARGS";
    }
}

class StudentParameterized {
    String name;
    StudentParameterized(String newName) {
       name = newName;
    }
}

public class Main {
    public static void main(String[] args) {
        StudentParameterized std = new StudentParameterized("newName");
        
        // because we have explicitely defined some constructor
        // in the class compiler doesn't create a default constructor
        // hence, we cannot initialize the object like this now
        StudentParameterized thisDoesnotCompile = new StudentParameterized();

    }    
}