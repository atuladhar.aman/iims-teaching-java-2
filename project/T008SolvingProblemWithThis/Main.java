
class Student {
    String name;
    Integer age;

    Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}

public class Main {
    public static void main(String[] args) {

        Student s = new Student("nama", 10);
        System.out.println("Name " + s.name + ", age " + s.age);
        
    }
}