interface Bank{  
    float rateOfInterest();  
}  
class NIBL implements Bank{  
    public float rateOfInterest(){return 9.15f;}  
}  
class NIC implements Bank{  
    public float rateOfInterest(){return 9.7f;}  
}  
public class Main{  
    public static void main(String[] args){  
        NIBL b=new NIBL();  
        System.out.println("ROI: "+b.rateOfInterest());  
    }
}  