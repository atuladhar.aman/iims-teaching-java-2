

class Student {
    static {
        System.out.println("static block student");
    }
    static void callMe() {
        System.out.println("static method student");
    }
}
public class Main {
    static {
        System.out.println("main static block");
    }
    public static void main(String[] args) {
        System.out.println("main");
        // Student.callMe();
        Student s = new Student();
    }
}