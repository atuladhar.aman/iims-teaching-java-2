

class Add_NumberOfParams {
    int add(int x, int y) {
        return x + y;
    }
    int add(int x, int y, int z) {
        return x + y + z;
    }
}


class Add_DiffDataType {
    int add(int x, int y) {
        return x + y;
    }
    float add(float x, float y) {
        return x + y;
    }
}
class Add_DiffOrderOfDataType {
    void add(int x, float y) {
        //body
    }
    void add(float x, int y) {
        //body
    }
}

public class Main {
    public static void main(String[] args) {
        Add_NumberOfParams obj = new Add_NumberOfParams();
        System.out.println(obj.add(1, 2));
        System.out.println(obj.add(1, 2, 3));

        Add_DiffDataType obj2 = new Add_DiffDataType();
        System.out.println(obj2.add(1, 2));
        System.out.println(obj2.add(1F, 2F));

        Add_DiffOrderOfDataType obj3 = new Add_DiffOrderOfDataType();
        obj3.add(1, 2F);
        obj3.add(1F, 2);
    }    
}