

abstract class Shape {
    // Who ever extends me
    // please give me body 
    abstract void draw();
}

class Triangle extends Shape {
    void draw() { System.out.println("Triangle drawn");}
}

class Rectangle extends Shape {
    void draw() { System.out.println("Rectange drawn");}
}