
class Person { static String person = "person"; }

class Teacher extends Person { static String teacher = "teacher"; }

class Student extends Person { static String student = "student"; }

public class Main {
    public static void main(String[] args) {
        System.out.println(Teacher.person);
        System.out.println(Teacher.teacher);
        System.out.println(Student.person);
        System.out.println(Student.student);
        // two lines below doesn't compile
        // System.out.println(Teacher.student);
        // System.out.println(Student.teacher);
    }
}

    

    