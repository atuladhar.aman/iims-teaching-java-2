
class Parent {
    void parentMethod() { System.out.println("parentMethod");}
}

class Child extends Parent{
    void childMethod() {
        super.parentMethod();
        System.out.println("child method");
    }
}

public class Main {
    public static void main(String[] args) {
        Child c = new Child();
        c.childMethod();
    }
}