

abstract class AbstractClass {

    String field;

    AbstractClass() {}

    void methodWithBody() { /* BODY */ }

    abstract void abstractMethod();
}