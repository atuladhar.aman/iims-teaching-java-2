

class Student {
    String name;
    static String collegeName = "IIMS";
}
public class Main {
    public static void main(String[] args) {
        Student s1 = new Student();
        ... // it doesn't matter how many
        ... // object we create, colleageName
        ... // is declared static, hence it
        ... // will allocate memory only once
        Student s1000 = new Student();
    }
}